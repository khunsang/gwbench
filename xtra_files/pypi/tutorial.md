
### [Packaging Python Projects](https://packaging.python.org/en/latest/tutorials/packaging-projects/#packaging-python-projects)

* Website (same link as the headline):
    https://packaging.python.org/en/latest/tutorials/packaging-projects/#packaging-python-projects
* This assumes that the project already contains `README.md` and `LICENSE` files.

First, upgrade `pip`
```
python3 -m pip install --upgrade pip
```

Next, a `pyproject.toml` is needed, e.g. for `gwbench`:
```toml
[build-system]
requires = ["hatchling"]
build-backend = "hatchling.build"

[project]
name = "gwbench"
version = "0.7.1"
authors = [
  { name="Ssohrab Borhanian", email="sborhanian@gmail.com" },
]
dependencies = [
    "astropy",
    "dill",
    "lalsuite",
    "mpmath",
    "numdifftools",
    "numpy",
    "pandas",
    "scipy",
    "sympy",
    "tqdm",
]
description = "A Python package for gravitational-wave benchmarking, particularly with Fisher information matrices."
readme = "README.md"
requires-python = ">=3.7"
classifiers = [
    "Programming Language :: Python :: 3",
    "License :: OSI Approved :: GNU General Public License v2 (GPLv2)",
    "Operating System :: OS Independent",
]

[project.urls]
"Homepage" = "https://gitlab.com/sborhanian/gwbench"
"Bug Tracker" = "https://gitlab.com/sborhanian/gwbench/-/issues"
```

Next, we install/upgrade `build` and `twine`
```
python3 -m pip install --upgrade build twine
```
and then `build` the package to generate archive files in the `dist` directory
```
python3 -m build
```

Next, generate an [API token](https://pypi.org/manage/account/#api-tokens) on PYPI. The use `twine` to upload all of the archives in `dist` using `__token__` as the username and the the API token for the password.

```
python3 -m twine upload --repository pypi dist/*
```
